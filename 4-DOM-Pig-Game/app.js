/*
GAME RULES:

- The game has 2 players, playing in rounds
- In each turn, a player rolls a dice as many times as he whishes. Each result get added to his ROUND score
- BUT, if the player rolls a 1, all his ROUND score gets lost. After that, it's the next player's turn
- The player can choose to 'Hold', which means that his ROUND score gets added to his GLBAL score. After that, it's the next player's turn
- The first player to reach 100 points on GLOBAL score wins the game

*/

var scores, activePlayer, currentScore, scoreToWin, diceDOM, gamePlaying;
window.onload = function(){
  newGame();
};


document.querySelector('.btn-roll').addEventListener('click', roll);

document.querySelector('.btn-hold').addEventListener('click', hold);

document.querySelector('.btn-new').addEventListener('click', newGame);



function changePlayer() {
  currentScore = 0;
  document.getElementById('current-' + activePlayer).textContent = currentScore;

  document.querySelector('.player-' + activePlayer + '-panel').classList.remove('active');

  // activePlayer = 1 - activePlayer / 1;
  activePlayer === 0 ? activePlayer = 1 : activePlayer = 0;

  document.querySelector('.player-' + activePlayer + '-panel').classList.add('active');
}

function newGame() {
  scores = [0, 0];
  activePlayer = 0;
  currentScore = 0;
  scoreToWin = 100;
  gamePlaying = true;
  diceDOM = document.querySelector('.dice');
  document.querySelector('.player-' + activePlayer + '-panel').classList.add('active');
  document.querySelector('.player-' + (activePlayer + 1)  + '-panel').classList.remove('active');

  document.querySelector('.dice').style.display = 'none';

  document.getElementById('score-0').textContent = 0;
  document.getElementById('score-1').textContent = 0;
  document.getElementById('current-0').textContent = 0;
  document.getElementById('current-1').textContent = 0;
}

function hold() {
    if(gamePlaying) {
  scores[activePlayer] += currentScore;
  document.getElementById('score-' + activePlayer).textContent = scores[activePlayer];

  if ( scores[activePlayer] >= scoreToWin) {
    alert('Player ' + activePlayer + ' win !!!');
    document.querySelector('.player-' + activePlayer + '-panel').classList.add('winner');
    document.querySelector('.player-' + activePlayer + '-panel').classList.remove('active');
    gamePlaying = false;
  } else{
        changePlayer();
  }
}
}

function roll() {
  if(gamePlaying) {

  // 1. Random numer
  var rollPoints = Math.floor(Math.random() * 6 ) + 1;

  // 2. Display result
  diceDOM.style.display = 'block';
  diceDOM.src = 'dice-' + rollPoints + '.png';

  // 3. Update the round
  if( rollPoints > 1) {
    currentScore += rollPoints;
    document.getElementById('current-' + activePlayer).textContent = currentScore;

  } else {
    changePlayer();
  }
}
}
