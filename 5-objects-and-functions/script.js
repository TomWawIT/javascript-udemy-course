//////////////////////////////////////////////////////
// Function constructor
// function Person(name, yearOfBirth, job) {
//   this.name = name;
//   this.yearOfBirth = yearOfBirth;
//   this.job = job;
// }
//
// Person.prototype.calculateAge = function() {
//   console.log( 2017 - this.yearOfBirth );
// }
//
// Person.prototype.lastName = 'Smith';
//
// var tom = new Person('Tom', 1991, 'Front-end Developer');


// Objet.create
// var personProto = {
//   calculateAge: function() {
//     console.log( 2016 - yearOfBirth);
//   }
// };
//
// var john = Object.create(personProto ,
//   {
//     name: { value : 'Tom'},
//     age: { value : 25 }
//   }
// );

//////////////////////////////////////////////////////
// Primitives vs objects

// Primitives:
// var a = 23;
// var b = a;
//
// a = 46;
//
// console.log(a);
// console.log(b);

//////////////////////////////////////////////////////
// Objects:
// var obj1 = {
//   name: 'John',
//   age: 25
// };
//
// var obj2 = obj1;
//
// obj1.age = 30;
//
// console.log(obj1);
// console.log(obj2);

//////////////////////////////////////////////////////
//Functions
// var age = 27;
// var obj = {
//   name: 'Tom',
//   city: 'Wroc'
// };
//
// function change (a, b) {
//   a  =  30;
//   b.city = "San Francisco"
// }
//
// change (age, obj);
//
// console.log(age);
// console.log(obj.city);


//////////////////////////////////////////////////////
// First Class Functions
//////////////////////////////////////////////////////
// Lecture: Passing functions as arguments
//
// var years = [1990, 1965, 1937, 2005, 1998];
//
// function arrayCalc(arr, fn) {
//   var arrRes = [];
//
//   for(var i = 0; i < arr.length ; i++) {
//     arrRes.push( fn(arr[i]) );
//   }
//
//   return arrRes;
// }
//
// function calculateAge(yearOfBirth) {
//   return 2017 - yearOfBirth;
// }
//
// function isFullAge(age) {
//   return age >= 18;
// }
//
// function maxHeartRate(el) {
//   if (el >= 18 && el <= 81) {
//       return Math.round( 206.9 - (0.67 * el) );
//   } else {
//     return -1;
//   }
// }
//
// var ages = arrayCalc(years, calculateAge);
// var fullAges = arrayCalc(ages, isFullAge);
// var rates = arrayCalc(ages, maxHeartRate);

//////////////////////////////////////////////////////
// Lecture: Functions Returning Functions
//
// function interviewQuestion(job) {
//   if ( job === 'designer' ) {
//     return function(name) {
//       console.log(name + ', can you please explain what UX design is?');
//     }
//   } else if ( job === 'teacher') {
//     return function(name) {
//       console.log('What subject do you teach, ' + name + '?');
//     }
//   } else {
//     return function(name) {
//       console.log('Hello ' + name + ' , what do you do?');
//     }
//   }
// }
//
//
// var teacherQuestion = interviewQuestion('teacher');
// var designerQuestion = interviewQuestion('designer');
//
// teacherQuestion('John');
// designerQuestion('John');
// designerQuestion('Mike');
//
// interviewQuestion('teacher')('Tom');


//////////////////////////////////////////////////////
// IIFE: Immediately Invoked Function Expresions
//////////////////////////////////////////////////////
//
// (function () {
//   var score = Math.random() * 10;
//   console.log(score >= 5);
// })();



//////////////////////////////////////////////////////
// Closures
//////////////////////////////////////////////////////
// An inner function has always acces to the variables and parameters of its outer function,
// even after the outer function has returned.

// function retirement(retirementAge) {
//   var a = ' years left until retirement.';
//   return function(yearOfBirth) {
//     var age = 2017 - yearOfBirth;
//     console.log( (retirementAge - age) + a );
//   }
// }
//
// var retirementUS = retirement(66);
// var retirementGermany = retirement(65);
// var retirementIceland = retirement(67);
//
// retirementUS(1991);
// retirementGermany(1991);
// retirementIceland(1991);


// rewriting interviewQuestion function form line 117 with closures :
// function interviewQuestion(job) {
//   return function(name) {
//     if ( job === 'designer' ) {
//        console.log(name + ', can you please explain what UX design is?');
//     } else if ( job === 'teacher') {
//        console.log('What subject do you teach, ' + name + '?');
//     } else {
//        console.log('Hello ' + name + ' , what do you do?');
//     }
//   }
// }
//
// interviewQuestion('designer')('Tom')


//////////////////////////////////////////////////////
// Bind, call and apply
//////////////////////////////////////////////////////

// var john = {
//   name: 'John',
//   age: 26,
//   job: 'teacher',
//   presentation: function(style, timeOfDay) {
//     if(style === 'formal') {
//       console.log('Good ' + timeOfDay + ', Ladis and Gentelmen! I\'m ' + this.name + ', I\'m a ' + this.job + ' and I\'m ' + this.age + ' years old.');
//     } else if (style === 'friendly') {
//       console.log('Hey! What\'s up? I\'m ' + this.name + ', I\'m a ' + this.job + ' and I\'m ' + this.age + ' years old. Have a nice ' + timeOfDay + '.');
//     }
//   }
// };
//
// var emily = {
//   name: 'Emily',
//   age: 35,
//   job: 'designer'
// }

// john.presentation('friendly', 'morning');
// john.presentation.call(emily, 'formal', 'afternoon' );
// john.presentation.apply(emily, ['formal', 'afternoon'] );  // array - doesn't work in this example

// var johnFriendly = john.presentation.bind(john, 'friendly');
// johnFriendly('morning');
// 
// var years = [1990, 1965, 1937, 2005, 1998];
//
// function arrayCalc(arr, fn) {
//   var arrRes = [];
//   for(var i = 0; i < arr.length ; i++) {
//     arrRes.push( fn(arr[i]) );
//   }
//   return arrRes;
// }
//
// function calculateAge(yearOfBirth) {
//   return 2017 - yearOfBirth;
// }
//
// function isFullAge(limit, age) {
//   return age >= limit;
// }
//
// var ages = arrayCalc(years, calculateAge);
// var fullJapan = arrayCalc(ages, isFullAge.bind(this, 20));
