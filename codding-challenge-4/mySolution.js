(function() {
  function Question (question, answers, correctAnswer) {
    this.question = question;
    this.answers = answers;
    this.correctAnswer = correctAnswer;
  }
  Question.prototype.isAnswerCorrect = function (userAnswer) {
    if (parseInt(userAnswer) == this.correctAnswer) {
      console.log('Correct answer!');
      currentScore++;
    } else {
      console.log('Wrong answer!');
    }
  };

  Question.prototype.printQuestion = function() {
    console.log( this.question );
    for (var i = 0; i < questionsArr.length - 1; i++  ) {
      console.log( i + ':' + this.answers[i] );
    }
  };

  var currentScore = 0;

  var questionsArr = [
    new Question('What\'s Your name 1?', ['Tom', 'Bob'], 0 ),
    new Question('What\'s Your name 2?', ['Tom', 'Bob'], 0 ),
    new Question('What\'s Your name 3?', ['Tom', 'Bob'], 0 )
  ];

  askNewQuestion();

  function askNewQuestion() {
    var r = Math.floor(Math.random() * questionsArr.length); // randomize number
    questionsArr[r].printQuestion(); // print randomized question
    var userAnswer = prompt('What is your answer?'); // ask user for answer
    questionsArr[r].isAnswerCorrect(userAnswer); // check if user answer was correct
    showResult(); // display current score
    if(userAnswer !== 'exit') {  // finish game if user type 'exit'
      askNewQuestion();
    }
  }

  function showResult() {
    console.log('----------------------');
    console.log('Your current score is ' + currentScore);
    console.log('----------------------');
  }

}());
